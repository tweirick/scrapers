import xml.etree.ElementTree as ET
import urllib.request
from urllib.parse import urlencode
import re
from time import gmtime, sleep, mktime, strptime, strftime
from sys import stderr, stdout
import argparse

start = 0
last_query = None
viewed_guids = set()
parser = argparse.ArgumentParser(description='')

parser.add_argument('--print-to-stdout', dest='print_to_stdout', action="store_true", default=False, help='')
parser.add_argument('--location', default='', help='')
parser.add_argument('--keyword',  default='', help='')
parser.add_argument('--radius',  dest='radius', type=int, default=0, help='Search radium (km)')
parser.add_argument('--max-age', dest='age',    type=int, default=90, help='Max listing age in days.')

args = parser.parse_args()

location, keyword = args.location, args.keyword
radius, age = args.radius, args.age
total_printed = 0
date_pattern = "%a, %d %b %Y %H:%M:%S %Z"

if args.print_to_stdout:
    out_file = stdout
else:
    file_name = "%s__l-%s__q-%s__radius-%s__fromage-%s.scrape.tsv" % (
        strftime("%Y%m%d%H%M%S", gmtime()), location, keyword, radius, age)
    out_file = open(file_name ,'w')

while True:
    url_dict = {'q': keyword,  'l': location, 'start': start, 'radius': radius, 'fromage': age }
    url_txt = "http://jp.indeed.com/rss?" + urlencode(url_dict)
    print("[LOG] QUERYING:", url_txt, file=stderr)
    start += 20
    guid_set = set()
    with urllib.request.urlopen(url_txt) as f:
        out_list = []
        root = ET.fromstring(f.read().decode('utf-8'))

        for item_el in root.iter('item'):
            title = item_el.find("title").text
            link = item_el.find("link").text
            source = item_el.find("source").text
            guid = item_el.find("guid").text
            pub_date = item_el.find("pubDate").text
            pub_date_epoch = str(int(mktime(strptime(pub_date, date_pattern))))
            description = item_el.find("description").text

            # Note for the future:
            # Access elements in itertree using numeric indexes e.g. root[0][9][6]
            georss_point = item_el.find('{http://www.georss.org/georss}point').text

            try:
                salary = re.search(r'([\d,]*[円万\s~]{0,}[\d,]*[万]*円)', description).group(1)
                # salary = re.search(r'給(\d*[万\s~]{0,}\d*[万]*)円', description).group(1)
            except AttributeError:
                salary = "None"

            guid_set.add(guid)
            out_list.append("\t".join([guid, title, salary, pub_date, pub_date_epoch, link, georss_point, description]))

    # ===========================================
    if len(guid_set & viewed_guids) == 0:
        print_len = len(out_list)
        total_printed += print_len
        print("WROTE %s ENTRIES. TOTAL PRINTED %s " % (print_len, total_printed), file=stderr)
        print("\n".join(out_list), file=out_file)
        viewed_guids = viewed_guids | guid_set
    else:
        print("%s ID OVERLAPS DETECTED. EXITING." % len(guid_set & viewed_guids), file=stderr)
        break
    sleep(2)


